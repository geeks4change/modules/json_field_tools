<?php

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 *
 * @see \Drupal\json_field\Plugin\views\field\JSONDataField
 */
function json_field_tools_field_views_data_alter(array &$data, FieldStorageConfigInterface $fieldStorage) {
  $applicableFieldTypes = ['json', 'json_native', 'json_native_binary'];
  $fieldName = $fieldStorage->getName();
  $newName = "{$fieldName}__twig";
  if (in_array($fieldStorage->getType(), $applicableFieldTypes)) {
    foreach ($data as $tableName => &$tableData) {
      // Only the main table has this, not the revision table.
      if ($tableData[$fieldName] ?? NULL) {
        // Copy and adapt the field handler.
        $tableData[$newName] = $tableData[$fieldName];
        $tableData[$newName]['title'] = $tableData[$fieldName]['title'] . ' (with Twig exposed JSON data)';
        $tableData[$newName]['title short'] = $tableData[$fieldName]['title short'] . ' (with Twig exposed JSON data)';
        $tableData[$newName]['field']['id'] = 'json_field_tools_expose_json_data_to_twig';
        $tableData[$newName]['field']['click sortable'] = FALSE;
      }
    }
  }
}
