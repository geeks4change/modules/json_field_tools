<?php

namespace Drupal\json_field_tools\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the JSON Field Webform formatter.
 *
 * @FieldFormatter(
 *   id = "json_field_tools_webform_formatter",
 *   label = @Translation("Webform submission display for JSON field"),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary",
 *   },
 * )
 */
class JsonFieldWebformFormatter extends FormatterBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->entityTypeManager = $container->get('entity_type.manager');
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  public static function defaultSettings() {
    return [
      'spec_type' => '',
      'spec' => '',
    ] + parent::defaultSettings();
  }

  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['module'][] = 'webform';
    return $dependencies;
  }

  public function getJsonValues(FieldItemListInterface $items): array {
    return array_map(
      fn(FieldItemInterface $item) => json_decode($item->get('value')
        ->getValue(), TRUE),
      iterator_to_array($items)
    );
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['spec_type'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Type'),
      '#description' => $this->t('How the webform is determied'),
      '#default_value' => $this->getSetting('spec_type'),
      '#options' => [
        'webform_id' => $this->t('Webform ID'),
        'webform_reference_field' => $this->t('Webform reference field'),
        'webform_reference_from_json_path' => $this->t('Webform reference from JSON path'),
      ],
      '#empty_value' => '',
    ];
    // @todo Someday make a shiny form for this.
    $form['spec'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spec'),
      '#default_value' => $this->getSetting('spec'),
    ];
    return $form;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($this->settings['spec_type'] === 'webform_id') {
      $webformId = $this->settings['spec'];
      $webform = Webform::load($webformId);
    }
    elseif ($this->settings['spec_type'] === 'webform_reference_field') {
      $webformReferenceFieldName = $this->settings['spec'];
      $entity = $items->getEntity();
      if (
        $entity->hasField($webformReferenceFieldName)
        && ($webformReferenceField = $entity->get($webformReferenceFieldName))
        && $webformReferenceField instanceof EntityReferenceFieldItemListInterface
      ) {
        $referencedEntity = $webformReferenceField->referencedEntities()[0] ?? NULL;
        if ($referencedEntity instanceof WebformInterface) {
          $webform = $referencedEntity;
        }
      }
    }
    elseif ($this->settings['spec_type'] === 'webform_reference_from_json_path') {
      $jsonPath = $this->settings['spec'];
      $pathItems = explode('.', $jsonPath);
      $jsonValues = $this->getJsonValues($items);
      $webformId = NestedArray::getValue($jsonValues, $pathItems);
      $webform = Webform::load($webformId);
    }

    if (!$webform) {
      return [];
    }

    $build = $this->viewNative($items, $webform->id());
    (new CacheableMetadata())
      ->addCacheableDependency($webform)
      ->applyTo($build);

    $build['#attached']['library'][] = 'json_field_tools/json_field_tools_webform_formatter';

    return $build;
  }

  protected function viewNative(FieldItemListInterface $items, string $webformId): array {
    // JSON field usually has only one item, but this works fur multiple too.
    $dataItems = $this->getJsonValues($items);
    // Create one or multiple webform submissions out of this.
    try {
      $webformSubmissions = array_map(
        fn(array $data) => WebformSubmission::create(['webform_id' => $webformId])
          ->setData($data),
        $dataItems
    );
    } catch (EntityStorageException $e) {
      return [];
    }

    $webformSubmissionViewBuilder = $this->entityTypeManager->getHandler('webform_submission', 'view_builder');
    assert($webformSubmissionViewBuilder instanceof EntityViewBuilderInterface);

    $build = $webformSubmissionViewBuilder->viewMultiple($webformSubmissions);
    // The resulting build is simply #theme=[entityId],...
    // @see \Drupal\webform\WebformSubmissionViewBuilder::getBuildDefaults
    foreach (Element::children($build) as $child) {
      // @see json_field_webform_formatter_webform_submission_view_alter()
      $build[$child]['#hide_empty'] = TRUE;
    }
    return $build;
  }

}
