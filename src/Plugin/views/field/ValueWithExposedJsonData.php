<?php

namespace Drupal\json_field_tools\Plugin\views\field;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\views\Annotation\ViewsField;
use Drupal\views\Plugin\views\field\EntityField;

/**
 * Field handler to present JSON data to an entity "data" display.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("json_field_tools_expose_json_data_to_twig")
 */
class ValueWithExposedJsonData extends EntityField {

  protected function documentSelfTokens(&$tokens) {
    parent::documentSelfTokens($tokens);
    $tokens['{{ ' . $this->options['id'] . '__data' . ' }}'] = $this->t('The raw data to use in twig.');
  }

  protected function addSelfTokens(&$tokens, $item) {
    parent::addSelfTokens($tokens, $item);
    $field = $item['raw'] ?? NULL;
    if ($field instanceof FieldItemInterface) {
      $value = $field->get('value')->getValue() ?? '[]';
      $data = json_decode($value, TRUE);
    }
    else {
      $data = [];
    }
    $tokens['{{ ' . $this->options['id'] . '__data }}'] = $data;
  }

}

