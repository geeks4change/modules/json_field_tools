<?php

declare(strict_types=1);
namespace Drupal\Tests\json_field_tools\Traits;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

trait SetupNodeTrait {

  protected NodeInterface $node;

  public function setupNode(): void {
    $this->node = Node::create([
      'type' => 'json_field_tools_test',
      'title' => 'Test',
      'field_json_field_tools_test_json' => json_encode([
        'text' => 'Hello',
        'checkbox' => 1,
      ]),
    ]);
    $this->node->save();
  }

}
