<?php

declare(strict_types=1);
namespace Drupal\Tests\json_field_tools\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\json_field_tools\Traits\SetupNodeTrait;

/**
 * @group json_field_tools
 */
final class JsonFieldToolsViewsTest extends BrowserTestBase {

  use SetupNodeTrait;

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'json_field_tools_test',
  ];

  protected function setUp(): void {
    parent::setUp();
    $this->setupNode();
  }

  public function testViewsField() {
    $this->drupalGet('/json-field-tools-test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('json.text="Hello"');
  }

}
