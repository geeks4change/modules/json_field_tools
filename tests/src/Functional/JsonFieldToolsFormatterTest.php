<?php

declare(strict_types=1);
namespace Drupal\Tests\json_field_tools\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\json_field_tools\Traits\SetupNodeTrait;

/**
 * @group json_field_tools
 */
final class JsonFieldToolsFormatterTest extends BrowserTestBase {

  use SetupNodeTrait;

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'json_field_tools_test',
  ];

  protected function setUp(): void {
    parent::setUp();
    $this->setupNode();
  }

  public function testFormatter() {
    $this->drupalGet($this->node->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Text Hello');
    $this->assertSession()->pageTextContains('Checkbox Yes');
  }

}
